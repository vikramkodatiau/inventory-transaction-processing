package com.mc.inventorymngmt.dto;

import javax.validation.constraints.NotNull;

public class ItemDto {
    @NotNull(message = "Item ID is Mandatory")
    private Integer id;

    @NotNull(message = "Quantity is Mandatory")
    private Integer quantity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
