package com.mc.inventorymngmt.dto;

import javax.validation.constraints.NotNull;

public class InventoryCreateDto {


    @NotNull(message = "Inventory Item Id is Mandatory")
    private Integer itemId;

    @NotNull(message = "Inventory Item Count Is is Mandatory")
    private Integer count;


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }
}
