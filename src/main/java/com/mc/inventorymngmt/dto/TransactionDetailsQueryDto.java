package com.mc.inventorymngmt.dto;

import com.mc.inventorymngmt.entity.TransactionItems;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@ApiModel("TransactionDetailsQueryDto")
public class TransactionDetailsQueryDto {

    @ApiModelProperty(
            notes = "Uniquely identifies the policy which is to be updated."
    )
    private Long transactionId;

    @ApiModelProperty(
            notes = "Uniquely identifies the policy which is to be updated."
    )
    private Integer correllationId;

    @ApiModelProperty(
            notes = "Uniquely identifies the policy which is to be updated."
    )
    private Integer merchantId;

    @ApiModelProperty(
            notes = "Uniquely identifies the policy which is to be updated."
    )
    private String apiUser;

    @ApiModelProperty(
            notes = "Uniquely identifies the policy which is to be updated."
    )
    private BigDecimal amount;

    @ApiModelProperty(
            notes = "Uniquely identifies the policy which is to be updated."
    )
    private String cardNumber;




    private List<TransactionItems> transactionItems;

    public TransactionDetailsQueryDto(Long transactionId, Integer correllationId, Integer merchantId, String apiUser, BigDecimal amount, String cardNumber, List<TransactionItems> transactionItems) {
        this.transactionId = transactionId;
        this.correllationId = correllationId;
        this.merchantId = merchantId;
        this.apiUser = apiUser;
        this.amount = amount;
        this.cardNumber = cardNumber;
        this.transactionItems = transactionItems;
    }
}
