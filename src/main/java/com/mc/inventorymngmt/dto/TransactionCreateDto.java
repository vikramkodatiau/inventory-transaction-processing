package com.mc.inventorymngmt.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class TransactionCreateDto {

    @NotNull(message = "Transaction Id is Mandatory")
    @Positive
    private Long transactionId;

    @NotNull(message = "Correlation Id is Mandatory")
    @Positive
    private Integer correllationId;

    @NotNull(message = "Merchant Id is Mandatory")
    @Positive
    private Integer merchantId;

    @NotNull(message = "ApiUser Id is Mandatory")
    @Length(min = 5, max = 10, message = "User name should be Minimum 5 and Max 10 characters")
    private String apiUser;

    @NotNull(message = "ApiPwd Id is Mandatory")
    @Length(min = 5, max = 8, message = "Password should be Minimum 5 and Max 8 characters")
    private String apiPwd;


    @NotNull(message = "Amount is Mandatory")
    @PositiveOrZero
    private BigDecimal amount;

    @NotNull(message = "Card Number is Mandatory")
    @Pattern(regexp = "\\d{4}?\\d{4}?\\d{4}?\\d{4}", message = "Enter a 16 digit card number")
    private String cardNumber;

    @NotNull(message = "ExpiryDate is Mandatory")
    private LocalDate expiryDate;

    @NotNull(message = "CSV number is Mandatory")
    @Size(min = 3, max = 3, message = "Enter a 3 digit csv number on back of your card")
    private String csv;

    @NotNull(message = "Item List is Mandatory")
    @Valid
    private List<ItemDto> itemDtoList;


    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getCorrellationId() {
        return correllationId;
    }

    public void setCorrellationId(Integer correllationId) {
        this.correllationId = correllationId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getApiUser() {
        return apiUser;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public String getApiPwd() {
        return apiPwd;
    }

    public void setApiPwd(String apiPwd) {
        this.apiPwd = apiPwd;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCsv() {
        return csv;
    }

    public void setCsv(String csv) {
        this.csv = csv;
    }

    public List<ItemDto> getItemDtoList() {
        return itemDtoList;
    }

    public void setItemDtoList(List<ItemDto> itemDtoList) {
        this.itemDtoList = itemDtoList;
    }


}
