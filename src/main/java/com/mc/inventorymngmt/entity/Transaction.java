package com.mc.inventorymngmt.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "transaction", uniqueConstraints =
@UniqueConstraint(columnNames = {"transaction_id", "merchant_id","correllation_id" }))
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "correllation_id")
    private Integer correllationId;
    @Column(name = "merchant_id")
    private Integer merchantId;
    @Column(name = "api_user")
    private String apiUser;
    @Column(name = "api_pwd")
    private String apiPwd;
    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "card_number")
    private String cardNumber;
    @Column(name = "expiry_date")
    private LocalDate expiryDate;
    @Column(name = "csv")
    private String csv;


    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getCorrellationId() {
        return correllationId;
    }

    public void setCorrellationId(Integer correllationId) {
        this.correllationId = correllationId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getApiUser() {
        return apiUser;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public String getApiPwd() {
        return apiPwd;
    }

    public void setApiPwd(String apiPwd) {
        this.apiPwd = apiPwd;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCsv() {
        return csv;
    }

    public void setCsv(String csv) {
        this.csv = csv;
    }

    public Long getId() {
        return id;
    }


}
