package com.mc.inventorymngmt.entity;

import javax.persistence.*;

@Entity
@Table(name = "inventory", uniqueConstraints =
@UniqueConstraint(columnNames = {"item_id", "merchant_id"}))
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "item_id")
    private Integer itemId;

    @Column(name = "merchant_id")
    private Integer merchantId;

    @Column(name = "count")
    private Integer count;

    public Inventory(Integer itemId, Integer merchantId, Integer count) {
        this.itemId = itemId;
        this.merchantId = merchantId;
        this.count = count;
    }

    public Inventory() {
    }


    public Integer getId() {
        return id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }
}
