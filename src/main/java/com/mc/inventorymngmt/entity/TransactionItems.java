package com.mc.inventorymngmt.entity;

import javax.persistence.*;

@Entity
@Table(name = "transaction_items", uniqueConstraints =
@UniqueConstraint(columnNames = {"item_id", "transaction_id"}))
public class TransactionItems {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "item_id")
    private Integer itemId;

    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "quantity")
    private Integer quantity;


    public TransactionItems(Integer itemId, Long transactionId, Integer quantity) {
        this.itemId = itemId;
        this.transactionId = transactionId;
        this.quantity = quantity;
    }
    public TransactionItems(){

    }


    public Integer getId() {
        return id;
    }



    public Integer getItemId() {
        return itemId;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getQuantity() {
        return quantity;
    }


}
