package com.mc.inventorymngmt.rest;

import com.mc.inventorymngmt.domain.InventoryManagementDomainService;
import com.mc.inventorymngmt.dto.InventoryCreateDto;
import com.mc.inventorymngmt.dto.TransactionCreateDto;
import com.mc.inventorymngmt.entity.Transaction;
import com.mc.inventorymngmt.error.InventoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
public class InventoryManafementController {

    @Autowired
    private final InventoryManagementDomainService domainService;

    public InventoryManafementController(InventoryManagementDomainService domainService) {
        this.domainService = domainService;
    }

    @GetMapping("api/transactions")
    public List<Transaction> retrieveTransactions() {
        return domainService.retrieveTransactions();
    }

    @GetMapping("api/transactions/{transactionId}")
    public Transaction getTransaction(@PathVariable(name = "transactionId") Long transactionId) {
        return domainService.getTransaction(transactionId);
    }

    @PostMapping("api/transactions")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> saveTransaction(@Valid @RequestBody TransactionCreateDto transactionDto) {

        try {
            domainService.saveTransaction(transactionDto);
        } catch (InventoryNotFoundException ex) {
            throw new InventoryNotFoundException(ex.getMessage());
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-ID", String.valueOf(transactionDto.getTransactionId()));
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PostMapping("api/merchant/{merchantId}/inventory")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public void createInventory(@PathVariable(name = "merchantId") Integer merchantId, @RequestBody InventoryCreateDto inventoryCreateDto) {
        domainService.createInventory(merchantId, inventoryCreateDto);
    }
}
