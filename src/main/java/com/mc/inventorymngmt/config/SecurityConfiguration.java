package com.mc.inventorymngmt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
                .withUser("user").password("{noop}user").roles("USER")
                .and()
                .withUser("admin").password("{noop}admin").roles("USER", "ADMIN");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/transactions/").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/api/transactions/{transactionId}").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/api/transactions/").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "api/merchant/{merchantId}/inventory").hasRole("ADMIN")
                .antMatchers("/h2/**").permitAll();

        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

}
