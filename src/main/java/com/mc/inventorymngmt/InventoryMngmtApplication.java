package com.mc.inventorymngmt;

import com.mc.inventorymngmt.entity.Inventory;
import com.mc.inventorymngmt.infra.InventoryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = "com.mc.inventorymngmt")
public class InventoryMngmtApplication {

    public static void main(String[] args) {
        SpringApplication.run(InventoryMngmtApplication.class, args);
    }

    @Bean
    CommandLineRunner initDatabase(InventoryRepository repository) {
        return args -> {
            repository.save(new Inventory(1, 100, 23));
            repository.save(new Inventory(2, 100, 34));
            repository.save(new Inventory(3, 110, 45));
            repository.save(new Inventory(4, 112, 34));
        };
    }

}
