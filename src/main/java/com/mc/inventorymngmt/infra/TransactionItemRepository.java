package com.mc.inventorymngmt.infra;

import com.mc.inventorymngmt.entity.Inventory;
import com.mc.inventorymngmt.entity.TransactionItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionItemRepository extends JpaRepository<TransactionItems, Long> {
    Optional<List<TransactionItems>> findByTransactionId(Long transactionId);

}
