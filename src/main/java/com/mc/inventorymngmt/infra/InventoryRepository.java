package com.mc.inventorymngmt.infra;

import com.mc.inventorymngmt.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {
    Optional<List<Inventory>> findByMerchantId(Integer merchantId);

    @Query(value = "select count(*) from inventory where item_id = ?1 and merchant_id =?2",  nativeQuery = true)
    Integer isExistsWithRecordWithCombination(Integer itemId, Integer merchantId);

}
